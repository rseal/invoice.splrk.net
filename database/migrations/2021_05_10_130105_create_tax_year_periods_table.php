<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTaxYearPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_year_periods', function (Blueprint $table) {
            $table->id();
            $table->integer('start_month')->nullable(false);
            $table->integer('start_day')->nullable(false);
            $table->integer('end_month')->nullable(false);
            $table->integer('end_day')->nullable(false);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('country_id', 2);

            $table->timestamps();
            $table->foreign('country_id')->references('id')->on('countries')->nullable(false);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_year_periods');
    }
}
