<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddTogglClientIds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('toggl_workspace_id');
            $table->unsignedBigInteger('toggl_client_id')->nullable();
        });

        DB::statement('UPDATE clients SET toggl_workspace_id = toggl_id WHERE toggl_type = \'workspaces\'');
        DB::statement('UPDATE clients SET user_id = 1');

        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('toggl_id');
            $table->dropColumn('toggl_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->integer('toggl_id')->nullable(false);
            $table->enum('toggl_type', ['workspaces', 'clients', 'projects'])->default('clients');
        });

        DB::statement('UPDATE clients SET toggl_id = toggl_workspace_id, toggl_type = \'workspaces\'');

        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('toggl_workspace_id');
            $table->dropColumn('toggl_client_id');
        });
    }
}
