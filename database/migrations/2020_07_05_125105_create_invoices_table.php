<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('number')->nullable(false);
            $table->date('since')->nullable(false);
            $table->date('until')->nullable(false);
            $table->json('toggl_report')->nullable(false);
            $table->foreignId('client_id')->nullable(false);
            $table->bigInteger('total_time');
            $table->double('total_bill', 8, 2);
            $table->string('currency')->default('USD');

            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
