<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class TaxYearConstraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP FUNCTION IF EXISTS is_valid_day_of_month');
        DB::statement('ALTER TABLE tax_year_periods ADD CONSTRAINT chk_start_month CHECK (start_month >= 1 AND start_month <= 12);');
        DB::statement('ALTER TABLE tax_year_periods ADD CONSTRAINT chk_start_day CHECK (' .
            '(start_month IN (4,6,9,11) AND start_day >= 1 AND start_day <= 30) ' .
            ' OR (start_month = 2 AND start_day>= 1 AND start_day <= 29) ' .
            ' OR (start_month IN (1,3,5,7,8,10,12) AND start_day >= 1 AND start_day <= 31))');
        DB::statement('ALTER TABLE tax_year_periods ADD CONSTRAINT chk_end_month CHECK (start_month >= 1 AND start_month <= 12);');
        DB::statement('ALTER TABLE tax_year_periods ADD CONSTRAINT chk_end_day CHECK (' .
            '(end_month IN (4,6,9,11) AND end_day >= 1 AND end_day <= 30) ' .
            ' OR (end_month = 2 AND end_day>= 1 AND end_day <= 29) ' .
            ' OR (end_month IN (1,3,5,7,8,10,12) AND end_day >= 1 AND end_day <= 31))');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE tax_year_periods DROP CONSTRAINT chk_start_month;');
        DB::statement('ALTER TABLE tax_year_periods DROP CONSTRAINT chk_start_day');
        DB::statement('ALTER TABLE tax_year_periods DROP CONSTRAINT chk_end_month');
        DB::statement('ALTER TABLE tax_year_periods DROP CONSTRAINT chk_end_day');
    }
}
