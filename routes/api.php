<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::apiResource('/clients', 'ClientController')->except(['delete']);

Route::apiResource('/invoices', 'InvoiceController')->only(['index', 'show']);

Route::get('countries/{country}/taxyear/next', 'CountryController@nextTaxYear');

Route::apiResource('countries', 'CountryController')->only(['index', 'show']);

Route::apiResource('taxyears', 'TaxYearController');

Route::apiResource('taxyearperiods', 'TaxYearPeriodController')->only(['index', 'show', 'store', 'update']);

Route::apiResource('payments', 'PaymentController');

Route::apiResource('exrates', 'ExchangeRateController')->only(['index', 'show']);