<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxYearPeriod extends Model
{
    protected $fillable = [
        'start_month',
        'start_day',
        'end_month',
        'end_day',
        'start_date',
        'end_date',
        'country_id',
    ];

    public function country() {
        return $this->belongsTo('App\Country');
    }
}
