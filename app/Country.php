<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use App\TaxYear;
use App\TaxYearPeriod;

class Country extends Model
{
    public $incrementing = false;

    public function createNextTaxYear() {
        $today = Date::today();
        $currentTaxYear = TaxYearPeriod::where('country_id', $this->id)
            ->where('start_date', '<=', $today->format('Y-m-d'))
            ->where('end_date', '>=', $today->format('Y-m-d'));
        
        $searchDate = $currentTaxYear ? Date::createFromTimeString($currentTaxYear->end_date) : $today;

        $currentPeriod = TaxYearPeriod::where('country_id', $this->id)
            ->where(function ($query) use ($searchDate) {
                $query->whereNull('start_date')
                    ->orWhere('start_date', '<=', $searchDate->format('Y-m-d'));
            })
            ->where(function ($query) use ($searchDate) {
                $query->whereNull('end_date')
                    ->orWhere('end_date', '>=', $searchDate->format('Y-m-d'));
            })
            ->orderBy('start_date')
            ->first();

        if ($currentPeriod) {
            $startDate = Date::create($searchDate->year, $currentTaxYear->start_month, $currentTaxYear->start_day);
            $endDate = Date::create($searchDate->year, $currentTaxYear->start_month, $currentTaxYear->start_day);

            return new TaxYear([
                'start_date' => $startDate,
                'end_date' => $endDate,
                'country_id' => $this->id,
            ]);
        }

        throw new \Exception('Unable to find a valid tax year period');
    }

    public function taxYears() {
        return $this->hasMany('App\TaxYear');
    }
}
