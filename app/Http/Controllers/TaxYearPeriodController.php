<?php

namespace App\Http\Controllers;

use App\Rules\CountryExists;
use App\Rules\IsValidDay;
use App\TaxYearPeriod;
use Illuminate\Http\Request;

class TaxYearPeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TaxYearPeriod::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'start_month' => 'required|gte:1|lte:12',
            'start_day' => ['required', new IsValidDay($request->start_month)],
            'end_month' => 'required|gte:1|lte:12',
            'end_day' => ['required', new IsValidDay($request->start_month)],
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date',
            'country_id' => ['required', new CountryExists],
        ]);

        $taxYearPeriod = new TaxYearPeriod($data);
        $taxYearPeriod->save();

        return $taxYearPeriod;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TaxYearPeriod  $taxYearPeriod
     * @return \Illuminate\Http\Response
     */
    public function show(TaxYearPeriod $taxYearPeriod)
    {
        return $taxYearPeriod;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaxYearPeriod  $taxYearPeriod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaxYearPeriod $taxYearPeriod)
    {
        $data = $request->validate([
            'start_month' => 'required|gte:1|lte:12',
            'start_day' => ['required', new IsValidDay($request->start_month)],
            'end_month' => 'required|gte:1|lte:12',
            'end_day' => ['required', new IsValidDay($request->start_month)],
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date',
            'country_id' => ['required', new CountryExists],
        ]);

        $taxYearPeriod->update($data);
        $taxYearPeriod->save();

        return $taxYearPeriod;
    }
}
