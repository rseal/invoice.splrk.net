<?php

namespace App\Http\Controllers;

use App\ExchangeRate;
use Illuminate\Http\Request;

class ExchangeRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $from = strtoupper($request->input('from'));
        $to = strtoupper($request->input('to'));

        $query = ExchangeRate::query();
        
        if ($from !== '') {
            $query->where('source_currency', $from);
        }

        if ($to !== '') {
            $query->where('destination_currency', $to);
        }

        return $query->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExchangeRate  $exchangeRate
     * @return \Illuminate\Http\Response
     */
    public function show(ExchangeRate $exchangeRate)
    {
        return $exchangeRate;
    }

}
