<?php

namespace App\Http\Controllers;

use App\TaxYear;
use Illuminate\Http\Request;
use App\Rules\CountryExists;

class TaxYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TaxYear::with(['country'])->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'title' => 'required|string',
            'country_id' => ['required', new CountryExists]
        ]);

        $taxYear = new TaxYear($data);

        $taxYear->save();
        return $taxYear;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TaxYear  $taxYear
     * @return \Illuminate\Http\Response
     */
    public function show(TaxYear $taxYear)
    {
        $taxYear->load(['country']);
        return $taxYear;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaxYear  $taxYear
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaxYear $taxYear)
    {
        $data = $request->validate([
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'title' => 'required|string',
            'country_id' => ['required', new CountryExists]
        ]);

        $taxYear->update($data);

        $taxYear->save();
        return $taxYear;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TaxYear  $taxYear
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaxYear $taxYear)
    {
        return $taxYear->delete();
    }

}
