<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Payment;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Payment::with(['invoice.client'])
            ->whereHas('invoice', function (Builder $query) {
                $query->where('user_id', Auth::user()->id);
            })
            ->orderBy('date')
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update', Invoice::find($request->invoice_id));

        $data = $request->validate([
            'invoice_id' => 'required|integer',
            'amount' => 'required|numeric',
            'date' => 'required|date',
        ]);

        $payment = new Payment($data);
        $payment->save();

        return $payment;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        $this->authorize('view', $payment);

        return $payment->load('invoice.client');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        $this->authorize('edit', $payment);

        $data = $request->validate([
            'invoice_id' => 'required|integer',
            'amount' => 'required|numeric',
            'date' => 'required|date',
        ]);

        $payment->update($data);
        $payment->save();

        return $payment;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        $this->authorize('delete', $payment);

        return $payment->delete();
    }
}
