<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxYear extends Model
{
    protected $fillable = [
        'start_date',
        'end_date',
        'title',
        'country_id',
    ];

    public function country() {
        return $this->belongsTo('App\Country');
    }
}