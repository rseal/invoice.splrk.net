<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'invoice_id',
        'amount',
        'date',
    ];

    public function invoice() {
        return $this->belongsTo('App\Invoice');
    }
}
