<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $casts = [
        'toggl_report' => 'array',
    ];

    public function client() {
        return $this->belongsTo('App\Client');
    }

    public function payments() {
        return $this->hasMany('App\Payment');
    }
}
