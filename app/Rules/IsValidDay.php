<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsValidDay implements Rule
{
    private int $startMonth;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($startMonth)
    {
        $this->startMonth = $startMonth;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        switch($this->startMonth) {
        case 4:
        case 6:
        case 9:
        case 11:
            return $value >= 1 && $value <= 30;
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return $value >= 1 && $value <= 31;
        case 2:
            return $value >= 1 && $value <= 29;
        default:
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid day of the month';
    }
}
