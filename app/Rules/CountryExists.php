<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Country;

class CountryExists implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !is_null(Country::where('id', $value)->get());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The country id is invalid.';
    }
}
