<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Invoice;
use App\Client;
use App\User;

class CreateInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:create {user_email} {start} {end} {client_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new invoice from Toggl data and add it to the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function generateInvoiceNumber(Client $client) {
        $lastInvoiceNumber = 0;
        $lastInvoice = Invoice::where('client_id', $client->id)->orderBy('number', 'desc')->first();

        if ($lastInvoice <> null) {
            $lastInvoiceNumber = $lastInvoice->number;
        }

        return $lastInvoiceNumber + 1;
    }

    private function timeToStr(int $milliseconds) {
        $hours = floor($milliseconds / 3600000);
        $minutes = floor(($milliseconds - ($hours * 3600000)) / 60000);

        return $hours.':'.str_pad(strval($minutes), 2, '0');
    }

    private function getTogglUserId($apiToken) {
        $response = Http::withBasicAuth($apiToken, 'api_token')->get('https://api.track.toggl.com/api/v8/me');
        return $response->json()['data']['id'];
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = User::firstWhere('email', $this->argument('user_email'));
        if (is_null($user)) {
            $this->error('User with email "' . $this->argument('user_email') . '" is not in the database');
            exit();
        }

        $startDate = $this->argument('start');
        $endDate = $this->argument('end');

        $client = Client::where('id', $this->argument('client_id'))->first();

        $userId = $this->getTogglUserId($user->togglApiToken);
        $response = Http::withBasicAuth($user->togglApiToken, 'api_token')->get(
            'https://api.track.toggl.com/reports/api/v2/summary',
            [
                'user_agent' => 'rseal@splrk.net',
                'since' => $startDate,
                'until' => $endDate,
                'workspace_id' => $client->toggl_workspace_id,
                'user_ids' => $userId,
                'client_id' => $client->toggl_client_id,
            ]
        );

        $togglReport = $response->json();

        $invoice = new Invoice;
        $invoice->client_id = $client->id;
        $invoice->since = $startDate;
        $invoice->until = $endDate;
        $invoice->user_id = $user->id;

        $invoice->total_time = 0;
        $invoice->total_bill = 0;
        $invoice->number = $this->generateInvoiceNumber($client);

        foreach($togglReport['data'] as &$project) {
            $project['billable_time'] = floor($project['time'] / 60000);
            $project['total_currencies'][0]['currency'] = 'USD';
            $project['total_currencies'][0]['amount'] = $project['billable_time'] * ($client->default_rate / 60.0);

            $invoice->total_bill += $project['total_currencies'][0]['amount'];
            $invoice->total_time += $project['billable_time'];

            $this->info('Adding Line Item:  "'.$project['title']['project'].'"  --- '.$this->timeToStr($project['time']));

            foreach($project['items'] as &$timeEntry) {
                $timeEntry['rate'] = $client->default_rate;
                $billable = floor($timeEntry['time'] / 60000);
                $timeEntry['sum'] = $billable * ($timeEntry['rate'] / 60.0);
            }
        }
        $invoice->toggl_report = $togglReport;

        $invoice->save();

        $this->info('Created new invoice for ' . $client->name . ' from ' . $startDate . ' until ' . $endDate . ' for $ ' . $invoice->total_bill);
    }
}
