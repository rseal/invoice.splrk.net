<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;
use App\ExchangeRate;
use App\Invoice;
use DateTime;

class PullSarsUSDRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:sars_usd_rates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape USD exchange rate from SARS website';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dom = new Crawler(
            Http::get('https://tools.sars.gov.za/rex/Rates/MultipleDefault.aspx')->body(),
            'https://tools.sars.gov.za/rex/Rates/MultipleDefault.aspx'
        );

        $firstDate = new DateTime(Invoice::orderBy('since', 'asc')->first()->since);
        $lastDate = new DateTime();

        $currentPage = 0;
        $rows = [];
        do {
            $currentPage += 1;
            $ratesForm = $dom->filterXPath('//form[@id="form1"]')->form();
            $ratesForm->setValues([
                // TODO: Fill in dates
                'ctl00$maincontent$dddateselection' => $firstDate->format('Y/m/d'),
                'ctl00$maincontent$dddatetorage' => $lastDate->format('Y/m/d'),
                'ctl00$maincontent$ddcountryselction' => 'United States America',
            ]);

            $args = array_merge(
                $ratesForm->getPhpValues(),
                $currentPage > 1 ?
                    [
                        '__EVENTTARGET' => 'ctl00$maincontent$gvExchangeRates',
                        '__EVENTARGUMENT' => 'Page$' . $currentPage,
                    ]
                    : ['ctl00$maincontent$btngetdata' => 'Get data'],
            );

            $ratesRes = Http::asForm()->post(
                $ratesForm->getUri(),
                $args
            );

            if ($ratesRes->ok()) {
                $dom = new Crawler($ratesRes->body(), $ratesForm->getUri());

                $rows = array_merge(
                    $rows,
                    $dom->filterXPath('//table[@id="maincontent_gvExchangeRates"]/tr[not(@class)]/td/..')->each(function (Crawler $node, $i) {
                        return [
                            'source_currency' => 'ZAR',
                            'destination_currency' => 'USD',
                            'rate' => doubleval($node->filter('td')->eq(5)->text()),
                            'valuation_date' => $node->filter('td')->eq(0)->text(),
                        ];
                    })
                );
            } else {
                $this->warn('Error pulling SARS USD exchange Rates: ' . $ratesRes->status());
            }

            $nextPages = $dom->filterXPath('//table[@id="maincontent_gvExchangeRates"]/tr[@class="GridPager"]/td//tr/td');
        } while (count($nextPages) > $currentPage);

        ExchangeRate::where('source_currency', 'ZAR')
            ->where('destination_currency', 'USD')
            ->where('valuation_date', '>=', $firstDate->format('Y-m-d'))
            ->where('valuation_date', '<=', $lastDate->format('Y-m-d'))
            ->delete();
    
        foreach($rows as $row) {
            $exr = ExchangeRate::create($row);
            $exr->save();
        }

        $this->table(
            ['From', 'To', 'Rate', 'Date'],
            ExchangeRate::select('source_currency', 'destination_currency', 'rate', 'valuation_date')
                ->where('source_currency', 'ZAR')
                ->where('destination_currency', 'USD')
                ->orderBy('valuation_date', 'asc')
                ->get()
        );


        // ExchangeRates::create($upsert, ['source_currency', 'destination_currency', 'valuation_date'], ['rate']);

        $this->info('Finished');
    }
}
