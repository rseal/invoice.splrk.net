<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\User;

class RegisterUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:register {name} {email} {togglApiToken} {phone}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getUserPassword() {
        $confirmedPassword = null;
        while(is_null($confirmedPassword)) {
            $password = $this->secret('New User\'s password:');
            $confirmationPassword = $this->secret('Confirm Password:');
            if ($password === $confirmationPassword) {
                $confirmedPassword = $password;
            } else {
                $this->warn('Passwords do not match');
            }
        }
        return $confirmedPassword;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (User::where('email', $this->argument('email'))->first()) {
            $this->error('User with email "' . $this->argument('email') . '" already exists');
            exit();
        }

        $user = new User();
        $user->name = $this->argument('name');
        $user->email = $this->argument('email');
        $user->togglApiToken = $this->argument('togglApiToken');
        $user->phone = $this->argument('phone');
        $user->address = '';
        $user->password = Hash::make($this->getUserPassword());
        $user->save();

        $this->info('Created user ' . $user->name . ' (' . $user->email . ') with id ' . $user->id);

        return 0;
    }
}
