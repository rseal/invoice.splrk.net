/**
 * Create an ISO date string (YYYY-mm-dd)
 * 
 * @param {Date} date 
 * @returns {string}
 */
export function formatDate(date) {
    let dateStr = date.getFullYear() + '-';
    dateStr += (date.getMonth() + 1).toString().padStart(2, '0') + '-';
    dateStr += date.getDate().toString().padStart(2, '0');

    return dateStr;
}