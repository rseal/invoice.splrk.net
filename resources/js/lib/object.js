
export const copyIds = (obj, defaultValue = null) => Object.keys(obj).reduce((final, key) => ({ ...final, [key]: defaultValue }), {});