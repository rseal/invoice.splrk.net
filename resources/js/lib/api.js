import { useState, useEffect, useCallback } from 'react';

function objectAsSnakeCase(data) {
  return Object.keys(data).reduce((sc, key) => {
    let scKey = key.replace(/[A-Z]/g, (match) => '_' + match.toLowerCase()).replace(/^_/, '');
    return {
      ...sc,
      [scKey]: data[key]
    }
  }, {});
}

export function useApiPost(url) {
  const [{ loading, errors, response }, setState] = useState({ loading: false, errors: [], response: false });
  const send = useCallback((data) => {
    setState(prevState => ({ ...prevState, errors: [], loading: true }));
    window.axios.post(url, objectAsSnakeCase(data), { withCredentials: true })
      .then(({ data }) => {
        setState({ loading: false, errors: [], response: data });
      })
      .catch((error) => {
        let errorMessages = [error.message];
        if (error.response) {
          errorMessages = error.response.data.json
        }
        setState({ loading, errors: errorMessages, response: false });
      });
  });

  return [send, loading, errors, response];
}

export function useApiQuery(url, defaultValue, deps = undefined) {
  const [{ loading, data, errorMessage }, setState] = useState({
    loading: true,
    data: defaultValue,
    errorMessage: ''
  });

  useEffect(() => {
      setState(prevState => ({ ...prevState, loading: true, errorMessage: '' }));
      window.axios.get(url, { withCredentials: true })
        .then(({ data }) => {
            setState({ loading: false, data, errorMessage: '' })
        })
        .catch(error => {
            console.error(error);
            if (error.response && error.response.status == 401) {
                window.location = '/login';
            } else {
                setState({ loading: false, data: defaultValue, errorMessage: error.message });
            }
        })
  }, deps);

  return [loading, data, errorMessage];
}
