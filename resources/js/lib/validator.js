import { useState, useMemo, useEffect, useCallback } from 'react';

export function lessThan(compare, customLabel = '', deps = []) {
    return useCallback((value, label) => value >= compare ? label + ' must be less than ' + (customLabel || compare) : false);
}

export function greaterThan(compare, customLabel = '', deps = []) {
    return useCallback((value, label) => value <= compare ? label + ' must be greater than ' + (customLabel || compare) : false, deps);
}

export function lessThanOrEqualTo(compare, customLabel = '', deps = []) {
    return useCallback((value, label) => value > compare ? label + ' must be less than or equal to ' + (customLabel || compare) : false, deps);
}

export function greaterThanOrEqualTo(compare, customLabel = '', deps = []) {
    return useCallback((value, label) => value < compare ? label + ' must be greater than or equal to ' + (customLabel || compare) : false, [compare]);
}

export function required(value, label) {
    return value === null || value === undefined || value === '' ? label + ' is required' : false;
}

export function validateCountry(countries) {
    return useCallback(value => !countries.find(({ id }) => id === value) ? 'Country not found' : false, [countries]);
}

export function useValidator(defaultValue, label, ...validations) {
    const [{ value, errors }, setState] = useState({ value: defaultValue, errors: [] });

    const setValue = (newValue) => {
        const newErrors = validations.map(validate => validate(newValue, label)).filter(error => !!error);
        setState({ value: newValue, errors: newErrors });
    };

    useEffect(() => {
        const newErrors = validations.map(validate => validate(value || defaultValue, label)).filter(error => !!error);
        setState((prevState) => ({ ...prevState, errors: newErrors }));
    }, validations);

    return [value, setValue, errors];
}

export function useErrorCheck(...errors) {
    return useMemo(() => !!errors.find(e => e.length > 0), errors);
}