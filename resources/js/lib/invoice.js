
export const formatInvoiceNumber = (prefix, number) => prefix + number.toString().padStart(4, '0');

export const findTaxYear = (taxYears, date) => taxYears.find(year => year.start_date <= date && year.end_date >= date);

export const paymentConvertToCurrency = (payment, currency, exRates) => {
    let exRate;
    let difference = Number.MAX_SAFE_INTEGER;
    let paymentDate = new Date(payment.date);
    for (let rate of exRates) {
        if ((
            rate.source_currency === payment.invoice.currency &&
            rate.destination_currency === currency
        ) || (
            rate.source_currency === currency &&
            rate.destination_currency === payment.invoice.currency
        )) {
            const rateDate = new Date(rate.valuation_date);
            let diff = Math.abs(rateDate - paymentDate);
            if (diff < difference) {
                difference = diff;
                exRate = rate;
            } else {
                break;
            }
        }
    }

    if (exRate) {
        if (exRate.source_currency === payment.invoice.currency) {
            return payment.amount * exRate.rate;
        } else {
            return payment.amount / exRate.rate;
        }
    }

    return payment.amount;
}