import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import './bootstrap';
import Invoices from './components/Invoices';
import Invoice from './components/Invoice';
import TaxYear from './components/TaxYear';
import TaxYearPeriod from './components/TaxYearPeriod';
import SideBar from './components/Sidebar';
import Payments from './components/Payments';

function App() {
  return (
    <Router>
      <SideBar />
      <Switch>
        <Route path="/invoices/:id">
          <Invoice />
        </Route>
        <Route path="/newtaxyear">
          <TaxYear />
        </Route>
        <Route path="/newtaxyearperiod">
          <TaxYearPeriod />
        </Route>
        <Route path="/payments">
          <Payments />
        </Route>
        <Route path="/">
          <Invoices />
        </Route>
      </Switch>
    </Router>
  );
}

if (document.getElementById('content')) {
    ReactDOM.render(<App />, document.getElementById('content'));
}
