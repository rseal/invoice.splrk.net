import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import Icon from 'react-evil-icons';
import PaymentForm from './PaymentForm';
import Loading from '../Loading';
import { useApiPost } from '../../lib/api';

export default function({ invoice, visible, afterSave, onClose }) {
    const [createPayment, paymentLoading, paymentError, paymentResponse] = useApiPost('/api/payments');

    useEffect(() => {
        if (paymentResponse && paymentError.length === 0) {
            afterSave();
        }
    }, [paymentResponse, paymentError]);

    return ReactDOM.createPortal(
        <div className={`full-view ${visible ? '' : 'hidden'}`}>
            <div className="overlay center-content">
                <div className="modal elevated-box" style={{ position: 'relative' }}>
                    <div onClick={onClose} style={{ position: 'absolute', top: '10px', right: '-10px' }}>
                        <Icon name="ei-close" size="s" />
                    </div>
                    {invoice && <PaymentForm invoice={invoice} onSave={createPayment} onCancel={onClose} />}
                    {paymentError.length > 0 && (
                        <div className="error-box">
                            {paymentError.map(message => <div key={message}>{message}</div>)}
                       </div>
                    )}
                    <Loading visible={paymentLoading} />
                </div>
            </div>
        </div>,
        document.getElementById('modal-window')
    );
}
