import React from 'react';
import { useValidator, useErrorCheck, greaterThanOrEqualTo, required } from '../../lib/validator';
import { formatInvoiceNumber } from '../../lib/invoice';
import FormField from '../FormField';

export default function PaymentForm({ invoice, onSave, onCancel }) {
    const [amount, setAmount, amountErrors] = useValidator(0.0, 'Amount', greaterThanOrEqualTo(0.0));
    const [date, setDate, dateErrors] = useValidator('', 'Date', required, greaterThanOrEqualTo(invoice.until, 'Invoice Last Date'));
    const formHasErrors = useErrorCheck(amountErrors, dateErrors);

    const onCreatePayment = (event) => {
        event.preventDefault();
        if (!formHasErrors) {
            onSave({
                invoiceId: invoice.id,
                date,
                amount
            });
        }
    }

    return (
        <form onSubmit={onCreatePayment}>
            <h3>New Payment for {formatInvoiceNumber(invoice.client.invoice_prefix, invoice.number)} by {invoice.client.name}</h3>
            <FormField
                id="date"
                label="Date"
                component="input"
                type="date"
                value={date}
                onChange={setDate}
                errors={dateErrors}
            />

            <FormField
                id="amount"
                label="Amount"
                component="input"
                type="number"
                step="0.01"
                min="0.0"
                value={amount}
                onChange={setAmount}
                errors={amountErrors}
            />

            <button type="submit">Save</button>
            <button onClick={onCancel}>Cancel</button>
        </form>
    );
}