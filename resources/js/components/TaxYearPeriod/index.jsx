import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import { useApiPost, useApiQuery } from '../../lib/api';
import Loading from '../Loading';
import TaxYearPeriod from './TaxYearPeriod';

export default function () {
    const [countriesLoading, countries] = useApiQuery('/api/countries', [], []);
    const [saveTaxYearPeriod, saveTaxYearPeriodIsLoading, saveTaxYearErrors, saveTaxYearResponse] = useApiPost('/api/taxyearperiods');

    const history = useHistory();

    const onCancel = () => history.goBack();

    useEffect(() => {
        if (saveTaxYearErrors.length === 0 && saveTaxYearResponse) {
            history.push('/');
        }
    }, [saveTaxYearErrors, saveTaxYearResponse]);

    return (
        <>
            <TaxYearPeriod countries={countries} onSave={saveTaxYearPeriod} onCancel={onCancel} />
            <Loading visible={countriesLoading || saveTaxYearPeriodIsLoading} />
        </>
    )
}