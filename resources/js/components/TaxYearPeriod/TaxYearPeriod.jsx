import React, { useMemo } from 'react';
import FormField from '../FormField';
import { useValidator, required, validateCountry, useErrorCheck } from '../../lib/validator';

const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

const maxDay = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

function MonthOptions(props) {
    return (
        <select {...props }>
            <option value="">Select Month</option>
            {months.map((month, index) => (
                <option key={month} value={index + 1}>{ month }</option>
            ))}
        </select>
    );
}

function DayOptions({ month, ...props }) {
    const maxDays = maxDay[month - 1] || 31
    const daysOfMonth = Array(maxDays).fill(0).map((_, i) => i + 1);
    return (
        <select {...props}>
            <option value="">Select Day</option>
            {daysOfMonth.map(day => (
                <option key={day} value={day}>{day}</option>
            ))}
        </select>
    );
}

export default function TaxYearPeriod({ countries, onSave, onCancel, errors }) {
    const [countryId, setCountryId, countryErrors] = useValidator(null, 'Country', required, validateCountry(countries));
    const [startMonth, setStartMonth, startMonthErrors] = useValidator(1, 'Start Month', required);
    const [startDay, setStartDay, startDayErrors] = useValidator(1, 'Start Month', required);
    const [endMonth, setEndMonth, endMonthErrors] = useValidator(1, 'End Month', required);
    const [endDay, setEndDay, endDayErrors] = useValidator(1, 'End Month', required);
    const [startDate, setStartDate, startDateErrors] = useValidator('', 'Start Date');
    const [endDate, setEndDate, endDateErrors] = useValidator('', 'End Date');

    const formHasErrors = useErrorCheck(countryErrors, startMonthErrors, startDayErrors, endMonthErrors, endDayErrors, startDateErrors, endDateErrors);

    const onSubmit = event => {
        event.preventDefault();
        if (!formHasErrors) {
            onSave({
                countryId,
                startMonth,
                startDay,
                endMonth,
                endDay,
                startDate,
                endDate,
            });
        }
    }

    return (
        <form onSubmit={onSubmit}>
            <FormField
                component="select"
                label="Country"
                errors={countryErrors}
                onChange={setCountryId}
                value={countryId || ''}
            >
                <option value="">Select Country</option>
                {countries.map(({ id, name }) => (
                    <option key={id} value={id}>{name}</option>
                ))}
            </FormField>

            <FormField
                component={MonthOptions}
                label="Start Month"
                id="start_month"
                errors={startMonthErrors}
                value={startMonth}
                onChange={setStartMonth || ''}
            />

            <FormField
                component={DayOptions}
                label="Start Day"
                id="start_day"
                errors={startDayErrors}
                value={startDay || ''}
                month={startMonth}
                onChange={setStartDay}
            />

            <FormField
                component={MonthOptions}
                label="EndMonth"
                id="end_month"
                errors={endMonthErrors}
                value={endMonth || ''}
                onChange={setEndMonth}
            />

            <FormField
                component={DayOptions}
                label="End Day"
                id="end_day"
                errors={endDayErrors}
                value={endDay || ''}
                month={endMonth}
                onChange={setEndDay}
            />

            <FormField
                component="input"
                label="Start Date"
                id="start_date"
                errors={startDateErrors}
                value={startDate || ''}
                type="date"
                onChange={setStartDate}
            />

            <FormField
                component="input"
                label="End Date"
                id="end_date"
                errors={endDateErrors}
                value={endDate || ''}
                type="date"
                onChange={setEndDate}
            />

            <button type="submit" disabled={formHasErrors}>Save</button>
            <button onClick={onCancel}>Cancel</button>
        </form>
    )
}