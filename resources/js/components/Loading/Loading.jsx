import React from 'react';
import Icon from 'react-evil-icons';

export default function Loading({ visible }) {
    return visible ? (
        <div className="overlay center-content">
            <Icon name="ei-spinner-3" size="l" />
        </div>
    ) : null
};
