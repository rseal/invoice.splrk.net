import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import Icon from 'react-evil-icons';

export default function SideBar() {
    return ReactDOM.createPortal(
        <>
            <Link to='/newtaxyear'>
                <Icon name="ei-plus" size="s" />
                Add Tax Year
            </Link>
            <Link to='/newtaxyearperiod'>
                <Icon name="ei-plus" size="s" />
                Add Tax Year Period
            </Link>
            <Link to="/payments">
                <Icon name="ei-credit-card" size="s" />
                Payments
            </Link>
        </>,
        document.getElementById('app-menu')
    );
}