import React from 'react';

/**
 * @param {Date} date
 * @returns {string}
 */
function formatInvoiceDate(date) {
  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  return `${date.getDate()} ${monthNames[date.getMonth()]} ${date.getFullYear()}`;
}

/**
 * @param {number} minutes
 */
function formatTime(minutes) {
  const hours = Math.floor(minutes / 60);
  const leftoverMinutes = (minutes - (hours * 60));

  return `${hours}:${leftoverMinutes.toString().padStart(2, '0')}`;
}

function currencyWithCommas(amount, fixed = 2) {
    return amount.toFixed(fixed).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default function Invoice({ invoice }) {
  const startDate = new Date(invoice.since);
  const endDate = new Date(invoice.until);

  return (
    <div className="page">
      <div id="header"></div>
      <h1>Invoice</h1>
      <div id="biller">
        <div className="name">Ryan Seal</div>
        <div className="email">rseal@splrk.net</div>
        <div className="phone">+1 719 751 8566</div>
      </div>
      <div id="billto">
        <h2>Bill To</h2>
        <div className="name">{invoice.client.name}</div>
        <h5>Invoice # {invoice.client.invoice_prefix}{invoice.number.toString().padStart(4, '0')}</h5>
      </div>
      <div id="dates">
        <div><b>{formatInvoiceDate(startDate)}</b> to <b>{formatInvoiceDate(endDate)}</b></div>
      </div>

      <div className="itemshead">
        Project
      </div>
      
      <div className="itemshead time">Time (HH:mm)</div>
      <div className="itemshead rate">Rate</div>
      <div className="itemshead amount">Amount</div>
      
      {invoice.toggl_report.data.map((item) => (
        <React.Fragment key={item.id}>
          <div className="item">{item.title.project}</div>
          <div className="item time">{formatTime(item.billable_time)}</div>
          <div className="item rate">$ {invoice.client.default_rate} / hr</div>
          <div className="item amount">$ {currencyWithCommas(item.total_currencies[0].amount)}</div>
        </React.Fragment>
      ))}

      <h4 id="total">Total</h4>
      <h4 id="total_time">{formatTime(invoice.total_time)}</h4>
      <h3 id="total_amount">$ {currencyWithCommas(invoice.total_bill)}</h3>

      <div id="footer"></div>
    </div>
  );
}