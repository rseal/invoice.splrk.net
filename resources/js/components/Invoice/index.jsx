import React from 'react';
import {useParams} from 'react-router-dom';
import { useApiQuery } from '../../lib/api';
import Loading from '../Loading';
import Invoice from './Invoice';

export default function InvoiceFromId() {
  const { id } = useParams();
  const [loading, invoice, errorMessage] = useApiQuery(
    `/api/invoices/${id}`,
    {},
    [id]
  );

  if (loading) {
    return <Loading visible />
  }

  if (errorMessage) {
    return <div className="error-box elevated-box">We're experiencing and unexpected Error right now.</div>
  }

  return <Invoice invoice={invoice} />
};
