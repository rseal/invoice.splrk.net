import React, { useState } from 'react';
import {useHistory} from 'react-router-dom';
import PaymentForm from '../PaymentForm';
import { copyIds } from '../../lib/object';
import { formatInvoiceNumber, findTaxYear } from '../../lib/invoice';
import Icon from 'react-evil-icons';

export default function Invoices({ invoices, countries, forceReloadInvoices }) {
  const lastTaxYears = countries.reduce((lty, country) => ({ ...lty, [country.id]: null }), {});
  const lastTaxYearsTotal = copyIds(lastTaxYears, 0);
  const history = useHistory();

  const [paymentModal, setPaymentModal] = useState({
    invoice: null,
    visible: false
  });

  const addPayment = (invoice) => {
    setPaymentModal({ invoice, visible: true });
  }

  const onClickAddPayment = (event) => {
    event.stopPropagation();
    addPayment(invoices[event.currentTarget.dataset.invoiceIndex]);
  }

  const closePaymentModal = () => {
    setPaymentModal({ invoice: null, visible: false });
    forceReloadInvoices();
  }

  return (
    <div>
      <table className="invoice-summary">
        <thead>
          <tr>
            <th>Invoice</th>
            <th>Client</th>
            <th>From</th>
            <th>To</th>
            <th>Total</th>
            <th>Paid</th>
            <th>Balance</th>
            {countries.map(country => (
              <th key={country.id}>{country.name}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {invoices.map((invoice, index) => {
            const paid = invoice.payments.reduce((total, { amount }) => total + amount, 0);
            return (
              <tr onClick={() => history.push(`/invoices/${invoice.id}`)}>
                <td>{formatInvoiceNumber(invoice.client.invoice_prefix, invoice.number)}</td>
                <td>{invoice.client.name}</td>
                <td>{invoice.since}</td>
                <td>{invoice.until}</td>
                <td>{invoice.total_bill}</td>
                <td>
                  {paid}
                  <div data-invoice-index={index} onClick={onClickAddPayment}>
                    <Icon name="ei-plus" size="s" />
                  </div>
                </td>
                <td>{ invoice.total_bill - paid}</td>
                {Object.values(countries).map(({ id: countryId, tax_years }) => {
                  const taxYear = findTaxYear(tax_years, invoice.until);
                  let newTaxYear = false;
                  if (taxYear !== lastTaxYears[countryId]) {
                    lastTaxYears[countryId] = taxYear;
                    lastTaxYearsTotal[countryId] = 0;
                    newTaxYear = index != 0;
                  }

                  lastTaxYearsTotal[countryId] += invoice.total_bill;
                  return <td key={invoice.id + '_' + countryId} className={newTaxYear ? 'tax-year-start' : ''}>{lastTaxYearsTotal[countryId]}</td>
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
      <PaymentForm invoice={paymentModal.invoice} visible={paymentModal.visible} afterSave={closePaymentModal} onClose={closePaymentModal} />
    </div>
  );
}
