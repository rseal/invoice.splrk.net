import React, { useCallback, useState } from 'react';
import Invoices from './Invoices';
import Loading from '../Loading';
import { useApiQuery } from '../../lib/api';

export default function InvoiceFromId() {
  const [lastReloadTime, setLastReloadTime] = useState(new Date());
  const [invoicesLoading, invoices, invoicesErrorMessage] = useApiQuery('/api/invoices', [], [lastReloadTime]);
  const [countriesLoading, countries, countriesErrorMessage] = useApiQuery('/api/countries', [], []);

  const forceReloadInvoices = useCallback(() => setLastReloadTime(new Date()), []);
  return (
    <>
      {(invoicesErrorMessage || countriesErrorMessage) && (
        <div class="error-box">We're experiencing and unexpected error and are unable to load invoices</div>
      )}
      <Invoices invoices={invoices} countries={countries} forceReloadInvoices={forceReloadInvoices} />
      <Loading visible={countriesLoading || invoicesLoading} />
    </>
  );
};
