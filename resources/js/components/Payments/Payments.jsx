import React from 'react';
import { copyIds } from '../../lib/object';
import { formatInvoiceNumber, findTaxYear, paymentConvertToCurrency } from '../../lib/invoice';

export default function Payments({ payments, countries, exchangeRates }) {
  const lastTaxYears = countries.reduce((lty, country) => ({ ...lty, [country.id]: null }), {});
  const lastTaxYearsTotal = copyIds(lastTaxYears, 0);

  return (
      <div>
          <table>
              <thead>
                  <tr>
                      <th>Date</th>
                      <th>Client</th>
                      <th>Invoice</th>
                      <th>Payment</th>
                      {countries.map(country => (
                          <th key={country.id}>{country.name} YTD</th>
                      ))}
                  </tr>
              </thead>
              <tbody>
                  {payments.map((payment, index) => (
                      <tr>
                          <td>{payment.date}</td>
                          <td>{payment.invoice.client.name}</td>
                          <td>{formatInvoiceNumber(payment.invoice.client.invoice_prefix, payment.invoice.number)}</td>
                          <td>{Intl.NumberFormat('en', { style: 'currency', currency: payment.invoice.currency }).format(payment.amount)}</td>
                          {countries.map(country => {
                              const taxYear = findTaxYear(country.tax_years, payment.date);
                              let newTaxYear = false;
                              if (taxYear !== lastTaxYears[country.id]) {
                                  lastTaxYears[country.id] = taxYear;
                                  lastTaxYearsTotal[country.id] = 0;
                                  newTaxYear = index != 0;
                              }
                              const paymentAmount = payment.invoice.currency === country.currency
                                ? payment.amount
                                : paymentConvertToCurrency(payment, country.currency, exchangeRates);

                              // TODO: move Int.NubmerFormat upwards
                              lastTaxYearsTotal[country.id] += paymentAmount;
                              return <td key={country.id} className={newTaxYear ? 'tax-year-start' : ''}>
                                  {Intl.NumberFormat('en-' + country.id, { style: 'currency', currency: country.currency }).format(lastTaxYearsTotal[country.id])}
                              </td>
                          })}
                      </tr>
                  ))}
              </tbody>
          </table>
      </div>
  )
}