import React from 'react';
import Payments from './Payments';
import Loading from '../Loading';
import { useApiQuery } from '../../lib/api';

export default function () {
    const [paymentsLoading, payments, paymentsErrorMessage] = useApiQuery('/api/payments', [], []);
    const [countriesLoading, countries, countriesErrorMessage] = useApiQuery('/api/countries', [], []);
    const [exRatesLoading, exRates, exRatesErrorMessage] = useApiQuery('/api/exrates', [], []);

    return (
        <>
            { (paymentsErrorMessage || countriesErrorMessage || exRatesErrorMessage) && (
                <div class="error-box">We're experiencing and unexpected error and are unable to load payments</div>
            )}
            <Payments payments={payments} countries={countries} exchangeRates={exRates} />
            <Loading visible={paymentsLoading || countriesLoading || exRatesLoading} />
        </>
    );
}
