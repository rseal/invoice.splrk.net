import React, { useState, useEffect, useMemo, useCallback } from 'react';
import { greaterThan, lessThan, useValidator, validateCountry, required, useErrorCheck } from '../../lib/validator';
import FormField from '../FormField';


const countryLabel = 'Country';

export default function TaxYear({ countries, onSave, onCancel, errors = [] }) {
    let [lessThanEndDate, setLessThanEndDate] = useState(() => () => {});
    let [greaterThanStartDate, setGreaterThanStartDate] = useState(() => () => {});

    const [countryId, setCountryId, countryErrors] = useValidator('', countryLabel, required, validateCountry(countries));
    const [startDate, setStartDate, startDateErrors] = useValidator('', 'Start Date', lessThanEndDate);
    const [endDate, setEndDate, endDateErrors] = useValidator('', 'End Date', greaterThanStartDate);
    const [{ custom: customTitle, title }, updateTitle, titleErrors] = useValidator(
        { custom: false, title: '' },
        'Title',
        useCallback(({ title }, label) => required(title, label), [title]),
    );
    const formHasErrors = useErrorCheck(countryErrors, startDateErrors, endDateErrors, titleErrors);

    useEffect(() => setLessThanEndDate(() => lessThan(endDate, 'End Date')), [endDate]);
    useEffect(() => setGreaterThanStartDate(() => greaterThan(startDate, 'Start Date')), [startDate]);

    useEffect(() => {
        if (!customTitle) {
            const currentCountry = countries.find(({ id }) => id === countryId);
            updateTitle({
                custom: false,
                title: (currentCountry?.name || '') + ' ' + (endDate ? endDate.slice(0, 4) : ''),
            });
        }
    }, [countryId, startDate, endDate]);

    const onSubmit = (event) => {
        event.preventDefault();
        if (!formHasErrors) {
            onSave({
                countryId,
                title,
                startDate: startDate,
                endDate: endDate,
            });
        }
    }

    const onChangeTitle = (title, event) => {
        updateTitle({ custom: title !== '', title }, event);
    }

    return (
        <form onSubmit={onSubmit}>
            {errors.length > 0 && (
                <ul>
                    {errors.map(error => <li>{error}</li>)}
                </ul>
            )}

            <FormField
                id="tax_year_country"
                label="Country"
                component="select"
                errors={countryErrors}
                onChange={setCountryId}
                value={countryId}
            >
                <option value="">Select Country</option>
                {countries.map(({ id, name }) => 
                    <option key={id} value={id}>{name}</option>
                )}
            </FormField>

            <FormField
                id="tax_year_title"
                label="Title"
                errors={titleErrors}
                component="input"
                type="text"
                value={title}
                onChange={onChangeTitle}
            />

            <FormField
                id="tax_year_start_date"
                label="Start Date"
                errors={startDateErrors}
                component="input"
                type="date"
                value={startDate}
                onChange={setStartDate}
            />

            <FormField
                id="tax_year_end_date"
                label="End Date"
                errors={endDateErrors}
                component="input"
                type="date"
                value={endDate}
                onChange={setEndDate}
            />

            <button type="submit" disabled={formHasErrors}>Save</button>
            <button onClick={onCancel}>Cancel</button>
        </form>
    );
}