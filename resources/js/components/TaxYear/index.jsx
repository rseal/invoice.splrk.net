import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Loading from '../Loading';
import TaxYear from './TaxYear';
import { useApiPost, useApiQuery } from '../../lib/api';

export default function () {
    const [loading, countries] = useApiQuery('/api/countries', [], []);
    const [createTaxForm, saveIsLoading, saveErrors, saveResponse] = useApiPost('/api/taxyears');

    const history = useHistory();

    const onCancel = () => history.goBack();

    useEffect(() => {
        if (saveErrors.length === 0 && saveResponse) {
            history.push('/');
        }
    }, [saveErrors, saveResponse]);

    return (
        <>
            <TaxYear countries={countries} onSave={createTaxForm} onCancel={onCancel} errors={saveErrors} />
            <Loading visible={loading || saveIsLoading} />
        </>
    );
}

