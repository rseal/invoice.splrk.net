import React from 'react';

export default function FormField({ id, label, errors = [], component, children, onChange, ...props }) {
    const Component = component;
    const onChangeValue = (event) => {
        onChange(event.target.value, event);
    }

    return (
        <>
            <label htmlFor={id}>{label}</label>
            {errors.length > 0 && (
                <ul>
                    {errors.map(error => <li key={error}>{error}</li>)}
                </ul>
            )}
            <Component onChange={onChangeValue} {...props}>
                {children}
            </Component>
        </>
    );
}
