import React, { useState, useEffect, useContext } from 'react';
import Loading from '../components/Loading';

const AuthContext = React.createContext({ access_token: '' });

export const client_id = '933c0f75-b7dc-4805-8ead-8fa3bc770a47';
const randomChars = 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890-._~';

function generateVerifier(length) {
    const verifier = Array(length).fill('').map(() => {
        return randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    });

    return verifier.join('');
}

async function generateChallenge(verifier) {
    const digest = await crypto.subtle.digest('SHA-256', new TextEncoder().encode(verifier));
    return btoa(
        String.fromCharCode(
            ...new Uint8Array(digest)
        )
    )
    .replace(/=/g, '')
    .replace(/\+/g, '-')
    .replace(/\//g, '_');
}

const AuthProvider = ({
    children,
    onLoadCache = () => ({ access_token: '' }),
    onSaveCache = () => {}
}) => {
    const [waiting, setWaiting] = useState(true);
    const [state, updateAuth] = useState(onLoadCache());

    useEffect(() => {
        if (!state.access_token && !sessionStorage.getItem('oauth_verifier')) {
            const verifier = generateVerifier(128);
            const state = generateVerifier(40);
            generateChallenge(verifier).then(code_challenge => {
                sessionStorage.setItem('oauth_verifier', verifier);
                sessionStorage.setItem('oauth_state', state);

                const query = new URLSearchParams({
                    response_type: 'code',
                    client_id,
                    code_challenge_method: 'S256',
                    code_challenge,
                    state,
                    redirect_uri: window.location.protocol + '//' + window.location.host + '/callback',
                });

                window.location = window.location.protocol + '//' + window.location.host + '/oauth/authorize?' + query;
            });
        }
    }, []);

    useEffect(() => {
        setWaiting(false);
        onSaveCache(state);
        if (state.access_token) {
            window.axios.defaults.headers.common.Authorization = 'Bearer ' + state.access_token;
        } else {
            delete window.axios.defaults.headers.common.Authorization;
        }
    }, [state]);

    return (
        <AuthContext.Provider value={{ waiting, ...state, updateAuth }}>
            { waiting
                ? <Loading visible />
                : children
            }
        </AuthContext.Provider>
    )
};

const AuthConsumer = AuthContext.Consumer;

const useAuth = () => useContext(AuthContext);

export {
    AuthContext,
    AuthProvider,
    AuthConsumer,
    useAuth,
};